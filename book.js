"use strict";
var mongoose = require("mongoose");
;
var bookSchema = new mongoose.Schema({
    title: String,
    description: String,
    authors: [{
            firstname: String,
            lastname: String
        }]
});
var Book = mongoose.model("Book", bookSchema);
module.exports = Book;
//# sourceMappingURL=book.js.map