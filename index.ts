import * as express from "express";
import * as bodyParser from "body-parser"
import * as Book from "./book"

var app = express();
var mongoose = require('mongoose');

mongoose.connect("mongodb://localhost/bookdb");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

/* Nova kniha */
app.post('/api/book', function (req, res) {
    var newBook = new Book(req.body);
    newBook.save((err)=>{
        if (err){
            res.json({info: 'error nepodarilo sa vytvorit knihu', error: err});
        }
        res.json({info: 'Kniha ulozena', data: newBook}); 
    });
});

/* Vsetky knihy */
app.get('/api/book', function (req, res) {
    Book.find((err, Books) => {
        if (err) {
            res.json({info: 'error nepodarilo sa najst knihy', error: err});
        };
        res.json({info: 'Najdene knihy', data: Books});
    });
});

/* Najdi knihu podla nazvu */
app.get('/api/book/title/:book', function (req, res) {
    var query = { title: req.params.book};
    Book.findOne(query, function(err, Book) {
        if (err) {
            res.json({info: 'error pri hladani kniny', error: err});
        };
        if (Book) {
            res.json({info: 'Kniha najdena', data: Book});
        } else {
            res.json({info: 'Nenasla sa kniha s nazvom:'+ req.params.book});
        }
    });
});

/* Najdi knihu podla id */
app.get('/api/book/id/:id', function (req, res) {
    var query = { _id: req.params.id};
    Book.findOne(query, function(err, Book) {
        if (err) {
            res.json({info: 'error pri hladani kniny', error: err});
        };
        if (Book) {
            res.json({info: 'Kniha najdena', data: Book});
        } else {
            res.json({info: 'Nenasla sa kniha s id:'+ req.params.id});
        }
    });
});

/* Najdi knihy podla opisu */
app.get('/api/book/desc/:desc', function (req, res) {
    var query = { description: req.params.desc};
    Book.find(query, function(err, Books) {
        if (err) {
            res.json({info: 'error pri hladani knih', error: err});
        };
        if (Books) {
            res.json({info: 'Kniha najdena', data: Books});
        } else {
            res.json({info: 'Nenasla sa kniha s opisom:'+ req.params.desc});
        }
    });
});

/* Najdi knihy podla priezviska autora */
app.get('/api/book/author/:lastname', function (req, res) {
    var query = { 'authors.lastname': req.params.lastname};
    Book.find(query, function(err, Books) {
        if (err) {
            res.json({info: 'error nepodarilo sa hladanie', error: err});
        };
        if (Books) {
            res.json({info: 'Knihy najdene', data: Books});
        } else {
            res.json({info: 'Kniha sa nenasla s priezviskom autora:'+ req.params.lastname});
        }
    });
});

/* Zmazanie knihy podla id */
app.get('/api/book/delete/:id', function (req, res) {
    var query = { _id: req.params.id};
    Book.findOneAndRemove(query, function(err, Book) {
        if (err) {
            res.json({info: 'error pri mazani knihy', error: err});
        };
        if (Book) {
            res.json({info: 'Kniha odstranena' + Book});
        } else {
            res.json({info: 'Nenasla sa kniha s id:'+ req.params.id});
        }
    });
});


    
/* Aktualizovanie podla id */
app.post('/api/book/update', function (req, res) {
    var updateBook = new Book(req.body);
    var query = { _id: req.body._id};
    Book.findOneAndUpdate(query, updateBook, function(err, Book) {
        if (err) {
            res.json({info: 'error pri aktualizovani knihy', error: err});
        };
        if (Book) {
            res.json({info: 'Kniha aktualizovana' + Book});
        } else {
            res.json({info: 'Nenasla sa kniha s id:'+ req.params.id});
        }
    });
});



var server = app.listen(3000, function () {
    console.log('Server listening on port 3000');
});