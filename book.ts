import * as mongoose from "mongoose";


interface IBook{
    title:string;
    description:string;
    [authors:number]:  { "firstname": string, "lastname": string };
}

interface IBookModel extends IBook, mongoose.Document{};
var bookSchema = new mongoose.Schema({
    title: String,
    description: String,
    authors: [{
    firstname : String,
    lastname : String
     }]
});

var Book = mongoose.model<IBookModel>("Book", bookSchema);

export = Book;